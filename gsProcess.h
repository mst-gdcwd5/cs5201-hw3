//Geoffrey Cline, gdcwd5
//CS5201, SS17, HW3
//gsProcess.h, definition of Grahm Schmidt Process class

#pragma once

#include "gVector.h"
#include <vector>
#include <cmath>


using namespace std;

class gsProcess
{
public:
    //Gram-Schmidt process for orthonormalizing set of vectors
    //Used MIT modified algorithm for increased numerical stability
    //Pre: a contains at least 1 vector of size at least 1
    //     no vector in a has a magnitude of zero
    //     operator*, multiplication, defined for T
    //     operator/, division, defined for T
    //     operator+=, addition, defined for T
    //     operator=, defined for T
    //     std::sqrt() defined for T
    //Pst: v updated to contain the same number and size of vectors as a
    //      however, updated with result of Gram-Schmidt Process
    template<class T> void operator()(const vector<gVector<T>> &a,  vector<gVector<T>> &v) const;

    //Gram-Schmidt process for orthonormalizing set of vectors
    //Used MIT classic algorithm for numerical stability
    //Pre: a contains at least 1 vector of size at least 1
    //     no vector in a has a magnitude of zero
    //     operator*, multiplication, defined for T
    //     operator/, division, defined for T
    //     operator+=, addition, defined for T
    //     operator=, defined for T
    //     std::sqrt() defined for T
    //Pst: v updated to contain the same number and size of vectors as a
    //      however, updated with result of Gram-Schmidt Process
    template<class T> void classic(const vector<gVector<T>> &a,  vector<gVector<T>> &v) const;

    //Gram-Schmidt process for orthonormalizing set of vectors
    //Used MIT modified algorithm for increased numerical stability
    //Pre: a contains at least 1 vector of size at least 1
    //     no vector in a has a magnitude of zero
    //     operator*, multiplication, defined for T
    //     operator/, division, defined for T
    //     operator+=, addition, defined for T
    //     operator=, defined for T
    //     std::sqrt() defined for T
    //Pst: v updated to contain the same number and size of vectors as a
    //      however, updated with result of Gram-Schmidt Process
    template<class T> void modified(const vector<gVector<T>> &a,  vector<gVector<T>> &v) const;

    //Gram-Schmidt process for orthonormalizing set of vectors
    //Used Price's formula for Gram-Schmidt, which has low numeric stability
    //Pre: a contains at least 1 vector of size at least 1
    //     no vector in a has a magnitude of zero
    //     operator*, multiplication, defined for T
    //     operator/, division, defined for T
    //     operator+=, addition, defined for T
    //     operator=, defined for T
    //     std::sqrt() defined for T
    //Pst: v updated to contain the same number and size of vectors as a
    //      however, updated with result of Gram-Schmidt Process
    template<class T> void price(const vector<gVector<T>> &a,  vector<gVector<T>> &v) const;
};

#include "gsProcess.hpp"