//Geoffrey Cline, gdcwd5
//CS5201, SS17, HW3
//gsProcess.hpp, implement of Grahm Schmidt Process class

#include "gsProcess.h"

template<class T>
void gsProcess::operator()(const vector<gVector<T>> &a,  vector<gVector<T>> &v) const
{
    return modified(a,v);
}

template<class T>
void gsProcess::modified(const vector<gVector<T>> &a,  vector<gVector<T>> &v) const
{

    if(a.size() < 1)
        throw std::runtime_error("gsProcess requires at least 1 input");

    auto sz = a.size();
    auto i = sz;
    auto j = sz;

    gVector<T>* q = new gVector<T> [sz];

    T** r = new T* [sz];
    for(i = 0; i < sz; i++)
        r[i] = new T [sz];

    v = a;

    if(v[0].mag() == 0)
        throw std::logic_error("magnitude of vector is zero");

    v[0] = v[0] / sqrt(v[0].mag());

    for(i = 0; i < sz; i++)
    {
        if(v[i].mag() == 0)
            throw std::logic_error("magnitude of vector is zero");
        r[i][i] = sqrt(v[i].mag());
        q[i] = v[i] / r[i][i];
        for(j = i + 1; j < sz; j++)
        {
            r[i][j] = q[i] * v[j];
            v[j] -= r[i][j] * q[i];
        }

    }

    for(i = 0; i < sz; i++)
        delete [] r[i];
    delete [] r;

    delete [] q;

    return;
}

template<class T>
void gsProcess::classic(const vector<gVector<T>> &a,  vector<gVector<T>> &v) const
{
    if(a.size() < 1)
        throw std::runtime_error("gsProcess requires at least 1 input");

    auto sz = a.size();
    auto i = sz;
    auto j = sz;

    gVector<T>* q = new gVector<T> [sz];

    T** r = new T* [sz];
    for(i = 0; i < sz; i++)
        r[i] = new T [sz];

    v.clear();

    for(j = 0; j < sz; j++)
    {
        v.push_back(a[j]);

        if(v[0].mag() == 0)
            throw std::logic_error("magnitude of vector is zero");

        if(j == 0)
            v[0] = v[0] / sqrt(v[0].mag());

        for(i = 0; i < j; i++)
        {
            r[i][j] = q[i]*a[j];
            v[j] = v[j]- r[i][j]*q[i];
        }

        if(v[j].mag() == 0)
            throw std::logic_error("magnitude of vector is zero");

        r[j][j] = sqrt(v[j].mag());
        q[j] = v[j] / r[j][j];
    }

    for(i = 0; i < sz; i++)
        delete [] r[i];
    delete [] r;

    delete [] q;

    return;
}

template<class T>
void gsProcess::price(const vector<gVector<T>> &a,  vector<gVector<T>> &v) const
{
    if(a.size() < 1)
        throw std::runtime_error("gsProcess requires at least 1 input");

    v = a;

    auto j = a.size();
    auto i = a.size();

    if(a[0].mag() == 0)
        throw std::logic_error("magnitude of vector is zero");

    v[0] = a[0] / sqrt(a[0].mag());

    for( j = 1; j < a.size(); j++)
    {
        v[j] = a[j];
        for( i = 0; i < j ; i++)
        {
            if(v[i].mag() == 0)
                throw std::logic_error("magnitude of vector is zero");
            v[j] -= ((v[0] * a[j]) / (v[i] * v[i])) * v[i];
        }
    }
}



//q is the projector
//how can implement projector?
//use modofied gs?

