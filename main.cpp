//Geoffrey Cline, gdcwd5
//CS5201, SS17, HW3
//main.cpp, driver for gsProcess class

#include <iostream>
#include "gsProcess.h"
#include "gVector.h"

int main(int argc, char *argv[])
{
    gsProcess func;
    vector<gVector<double>> idata;
    vector<gVector<double>> rdata;
    double result;

    //threshold for which doubles will be output as zero
    const double SMALL_THRESHOLD = 0.00000001;
    const int COUT_PRECISION = 10;

    string ifname;

    std::cout << "# Hello, World Y'All" << std::endl;

    //uses default file if not specified
    if( argc < 2)
    {
        ifname = "data/t3.txt";
    }
    else
        ifname = argv[1];

    cout << "# Opening File: " << ifname << endl;

    try
    {
        fileInput(ifname, idata);

        std::cout.precision(COUT_PRECISION);

        //output data read in
        cout << "# input" << endl;
        for(auto const &vec : idata)
        {
            cout << vec << endl;
        }

        func(idata, rdata);

        //output results of MIT-Modified Gram-Schmidt algorithm
        cout << "# modified" << endl;
        for(auto const &vec : rdata)
        {
            cout << vec << endl;
        }

        //outputs dot products of results
        auto i = rdata.size();
        auto j = rdata.size();
        cout << "# dot products" << endl;
        for( i = 0; i < rdata.size(); i++)
        {
            for( j = i+1; j < rdata.size(); j++)
            {
                if(i != j)
                {
                    result = rdata[i] * rdata[j];

                    cout << "# (" << i << "," << j << "): ";
                    cout << (abs(result) < SMALL_THRESHOLD ? "true" : "false") << endl;

                    cout << (abs(result) < SMALL_THRESHOLD ? 0 : result) << endl;
                }
            }
        }
    }
    catch(const std::runtime_error& e)
    {
        cout << "# runtime error: " << e.what() << endl;
    }
    catch(const std::logic_error& e)
    {
        cout << "# logic error: "  << endl;
    }

    
}
